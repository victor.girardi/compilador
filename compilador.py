import sys

# Program ::= Command { Command }
# Command ::= AssignCommand | GetCommand |
# AddCommand | SubCommand | MultCommand |
# DivCommand | ModCommand | PrintCommand |
# IfCommand | WhileCommand | CompositeCommand

# AssignCommand ::= “=” Variable Value
# GetCommand ::= “G” Variable
# AddCommand ::= “+” Variable Value Value
# SubCommand ::= “-” Variable Value Value
# MultCommand ::= “*” Variable Value Value
# DivCommand ::= “/” Variable Value Value
# ModCommand ::= “%” Variable Value Value
# PrintCommand ::= “P” Value
# Comparison ::= Variable Operator Value
# Operator ::= “=” | “<” | “#”
# IfCommand ::= “I” Comparison Command
# WhileCommand ::= “W” Comparison Command
# CompositeCommand ::= “{” Command { Command } “}”
# Value ::= Variable| Number
dicionarioDeSintaxe = {
    'ASSIGNCOMMAND': ['ID','VALUE'],
    'GETCOMMAND': ['ID'],
    'ADDCOMMAND': ['ID', 'VALUE', 'VALUE'],
    'SUBCOMMAND': ['ID', 'VALUE', 'VALUE'],
    'MULTCOMMAND': ['ID', 'VALUE', 'VALUE'],
    'DIVCOMMAND': ['ID', 'VALUE', 'VALUE'],
    'MODCOMMAND': ['ID', 'VALUE', 'VALUE'],
    'PRINTCOMMAND': ['VALUE'],
    'IFCOMMAND': ['ID', 'OPERATOR', 'VALUE', 'STARTCOMPOSITECOMMAND'],
    'WHILECOMMAND': ['ID', 'OPERATOR', 'VALUE', 'STARTCOMPOSITECOMMAND'],
    'ENDCOMPOSITECOMMAND': []
}

codigoFileName = ''
dicionarioDeTokens = {
    '$': 'ASSIGNCOMMAND',
    '=': 'OPERATOR',
    'G': 'GETCOMMAND',
    '+': 'ADDCOMMAND',
    '-': 'SUBCOMMAND',
    '*': 'MULTCOMMAND',
    '/': 'DIVCOMMAND',
    '%': 'MODCOMMAND',
    'P': 'PRINTCOMMAND',
    '<': 'OPERATOR',
    '#': 'OPERATOR',
    'I': 'IFCOMMAND',
    'W': 'WHILECOMMAND',
    '{': 'STARTCOMPOSITECOMMAND',
    '}': 'ENDCOMPOSITECOMMAND',
}

class compilador:
    sequenciaDeTokens = []

    def __init__(self, fileName):
        self.file = open(fileName, 'r')
        self.codigo = self.file.readlines()
        self.analizeLexica()

    #==================================================
    # INICIO ANALIZADOR LEXICO   
    #==================================================
    def analizeLexica(self):
        print('\n==============================')
        print('||          TOKENS          ||')
        print('==============================\n')
        self.gerarTokens()
        print('\n==============================')
        print('||        FIM TOKENS        ||')
        print('==============================')

        print('\n==============================')
        print('||        SINTATICA         ||')
        print('==============================\n')
        self.analizeSintatica()
        print('\n==============================')
        print('||       FIM SINTATICA      ||')
        print('==============================')


    def gerarTokens(self):
        for line in self.codigo:
            linhaDeTokens = self.analizarLinha(line)
            self.sequenciaDeTokens.append(linhaDeTokens)
        

    def analizarLinha(self, linha):
        linhaSemBreakLine = linha.rstrip('\n')
        print(f'LINHA: {linhaSemBreakLine}')
        linhaSemBreakLine = self.tratarCasosEspeciais(linhaSemBreakLine)

        linhaDeTokens = []
        for letra in linhaSemBreakLine:
            token = self.analizarLetra(letra)
            if token != None:
                linhaDeTokens.append(token)
        return linhaDeTokens

    def tratarCasosEspeciais(self, linha):
        newLine = linha
        if (newLine[0] == '='):
            newLine = '$' + newLine[1:]
        return newLine
            
    
    def analizarLetra(self, letra):
        if letra != ' ':
            print('  '+ (letra if letra != '$' else '=') + ' - ',end='')
            if letra.islower():
                print('ID')
                token = f'ID({letra})'
            elif letra in dicionarioDeTokens:
                print(dicionarioDeTokens[str(letra)])
                token = dicionarioDeTokens[str(letra)]
            elif letra.isnumeric():
                print('INT')
                token = f'INT({letra})'
            else:
                print(f'Nao sei o que é isso [{letra}]')
                token = 'ERROR'
            return token
        
    #==================================================
    # FIM ANALIZADOR LEXICO      
    #==================================================

    #==================================================
    # INICIO ANALIZADOR SINTATICO
    #==================================================

    def analizeSintatica(self):
        for linha in self.sequenciaDeTokens:
            self.analizarSintaxeDaLinha(linha)
    
    def analizarSintaxeDaLinha(self, linha):
        sintaxeEsperada = []
        stringHelper = ' '

        if linha[0] in dicionarioDeSintaxe:
            sintaxeEsperada = dicionarioDeSintaxe[str(linha[0])]
            isSintaxeCerta = self.checkarSeSintaxeEstaCerta(sintaxeEsperada, linha[1:])
            if (isSintaxeCerta):
                print(f"SINTAXE CERTA  \n  {stringHelper.join(linha)}")
            else:
                print(f"SINTAXE ERRADA \n  {stringHelper.join(linha)}")
        else:
            #ERROR DE SINTAXE
            print('ERROR DE SINTAXE')

    def checkarSeSintaxeEstaCerta(self, sintaxeEsperada, sintaxe):
        if len(sintaxeEsperada) == 0 and len(sintaxe) == 0:
            return True
        elif len(sintaxeEsperada) != len(sintaxe):
            return False

        for x in range(0, len(sintaxeEsperada)):
            if sintaxeEsperada[x] == 'VALUE':
                if sintaxe[x][0:2] == 'ID' or sintaxe[x][0:3] == 'INT':
                    continue
            elif sintaxeEsperada[x] == 'ID':
                if sintaxe[x][0:2] == sintaxeEsperada[x]:
                    continue
            elif sintaxeEsperada[x] == 'INT':
                if sintaxe[x][0:3] == sintaxeEsperada[x]:
                    continue
            elif sintaxe[x] == sintaxeEsperada[x]:
                continue
            else:
                print(f'ISSO ESTA ERRADO => {sintaxe[x]} ESPERADO {sintaxeEsperada[x]}')
                return False
        return True
        
    #==================================================
    # FIM ANALIZADOR SINTATICO
    #==================================================

if __name__ == "__main__":
    codigoFileName = sys.argv[1]
    compilador(codigoFileName)